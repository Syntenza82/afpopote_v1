-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 31 mars 2021 à 14:13
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `afpacar`
--

-- --------------------------------------------------------

--
-- Structure de la table `center`
--

DROP TABLE IF EXISTS `center`;
CREATE TABLE IF NOT EXISTS `center` (
  `id_center` int(11) NOT NULL AUTO_INCREMENT,
  `center_name` varchar(255) NOT NULL,
  `center_phoneNumber` varchar(20) NOT NULL,
  `center_address` varchar(255) NOT NULL,
  `center_complement_address` varchar(255) DEFAULT NULL,
  `center_zipCode` varchar(5) NOT NULL,
  `center_city` varchar(255) NOT NULL,
  `center_contact_mail` varchar(255) NOT NULL,
  `center_urlGoogleMap` varchar(255) NOT NULL,
  PRIMARY KEY (`id_center`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `center`
--

INSERT INTO `center` (`id_center`, `center_name`, `center_phoneNumber`, `center_address`, `center_complement_address`, `center_zipCode`, `center_city`, `center_contact_mail`, `center_urlGoogleMap`) VALUES
(1, 'AFPA Saint-Jean-de-Védas', '0467985632', '12 rue Jean Mermoz', 'Zone Industriel de La Lauze', '34430', 'Saint-Jean-de-Védas', 'afpa.vedas@contact.com', 'https://www.google.com/maps/dir/43.6007954,3.8757129/afpa+saint+jean+de+vedas/@43.5814544,3.8471799,14z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0x12b6b1e78790b019:0x5fe2ea1bc7b758d9!2m2!1d3.8450793!2d43.5645741'),
(2, 'AFPA Toulouse', '0499076223', '1 allée Griffon', '', '31400', 'Toulouse', 'afpa.toulouse@contact.com', 'https://www.google.com/maps/dir//afpa+toulouse/data=!4m6!4m5!1m1!4e2!1m2!1m1!1s0x12aebc61977f04b5:0xa2d0bb5b5e267d3a?sa=X&ved=2ahUKEwjbk4Pbu7HtAhUk5eAKHd_0CcYQ9RcwE3oECCIQBA');

-- --------------------------------------------------------

--
-- Structure de la table `parameter`
--

DROP TABLE IF EXISTS `parameter`;
CREATE TABLE IF NOT EXISTS `parameter` (
  `id_parameter` int(11) NOT NULL AUTO_INCREMENT,
  `id_center` int(11) NOT NULL,
  `parameter_name` varchar(255) NOT NULL,
  `parameter_value` varchar(255) DEFAULT NULL,
  `parameter_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_parameter`),
  KEY `parameter_center_FK` (`id_center`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `parameter`
--

INSERT INTO `parameter` (`id_parameter`, `id_center`, `parameter_name`, `parameter_value`, `parameter_status`) VALUES
(1, 1, 'fake_param', 'fake_param_value', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_center` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_firstname` varchar(255) NOT NULL,
  `user_mail` varchar(255) NOT NULL,
  `user_identifier` varchar(20) NOT NULL,
  `user_phoneNumber` varchar(20) NOT NULL,
  `user_pwd` varchar(255) NOT NULL,
  `user_role` int(11) DEFAULT '0',
  `user_date_last_connection` datetime DEFAULT NULL,
  `user_validation_code` varchar(6) NOT NULL,
  `user_gender` int(11) NOT NULL,
  `user_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_user`),
  KEY `user_center_FK` (`id_center`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `id_center`, `user_name`, `user_firstname`, `user_mail`, `user_identifier`, `user_phoneNumber`, `user_pwd`, `user_role`, `user_date_last_connection`, `user_validation_code`, `user_gender`, `user_status`) VALUES
(1, 1, 'Pagan', 'Jean-Jacques', 'pagan.jijou@gmail.com', '2154987654', '0636231456', 'Poos654654usnks', 4, '2021-03-04 11:41:17', 'BDL398', 1, 1),
(2, 1, 'Dupont', 'Charles', 'charles_dupont@orange.com', '98765431', '0658961234', 'RdhghdJ654OIUlkj', 0, '2021-02-24 08:00:00', 'DTHD65', 1, 1),
(3, 1, 'Maya', 'Amelia', 'amelia.maya@sfr.com', '3215687', '0679641322', 'ZHGS5465SLKJlk', 0, '2021-03-03 14:36:39', 'THS965', 0, 1),
(4, 1, 'Pitt', 'Brad', 'brad.pitt@free.fr', '95135789', '0497463162', 'GJHK654DV', 0, '2021-02-24 08:00:00', 'ULKJ69', 1, 1),
(5, 1, 'Grember', 'Damien', 'dgrember@gmail.com', '123456789', '0632993386', 'RMCK2BJUS', 1, '2021-03-02 08:42:53', 'MDRP42', 1, 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `parameter`
--
ALTER TABLE `parameter`
  ADD CONSTRAINT `parameter_center_FK` FOREIGN KEY (`id_center`) REFERENCES `center` (`id_center`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_center_FK` FOREIGN KEY (`id_center`) REFERENCES `center` (`id_center`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
