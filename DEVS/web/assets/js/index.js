/**
 * Show the loading modal
 */
 function showLoadingModal() {
	$('#loadingModal').show();
}

/**
 * Hide the loading modal
 */
function hideLoadingModal() {
	$('#loadingModal').hide();
}


$(function() {
    console.log('Corp de page : JS chargé');
    showLoadingModal();
    toastr.info("Deux petites secondes svp, je digère...mon américain", '...', 2000)
    setTimeout(
        function() {
            hideLoadingModal();
            toastr.success("Merci d'avoir patienté :)", 'Bienvenue')
        },
        2000
    )
   
});

